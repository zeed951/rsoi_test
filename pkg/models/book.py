from ..database import DB
from pprint import pprint
import datetime

class Book(object):
    def __init__(self, name):
        self.name = name
        self.created_date = str(datetime.datetime.utcnow())

    def insert(self):
        print("debug message book.insert 1")
        pprint(DB.find_one("books", {"name": self.name}))
        if DB.find_one("books", {"name": self.name}) == "null":
            print("debug message book.insert 2")           
            DB.insert(collection='books', data=self.json())

    def find_one(self):
        return DB.find_one("books", {"name": self.name})

    def delete(self):
        if DB.find_one("books", {"name": self.name}):
            DB.delete("books", {"name": self.name})
    
    def find_all(self):
        return DB.find_all('books')

    def json(self):
        return {
            'name': self.name,
            'created_date': self.created_date
        }
    def get_name(self):
        return self.name


if __name__ == "__main__":
    new_book = Book("book_new")
    # new_book.insert()
    print(new_book.find_all())
