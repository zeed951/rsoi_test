from ..models.book import Book

def func(x):
    test_book = Book(x)
    print(test_book.get_name())
    name = test_book.get_name()
    return name

def test_answer():
    print()
    assert func("test") == "test"