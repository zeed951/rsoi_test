import pymongo
import pprint
import json
from bson import ObjectId
from bson.json_util import loads

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            # print("encoder debug message!\n")
            return str(o) 
        return json.JSONEncoder.default(self, o)

class DB(object):

    URI = "mongodb+srv://new_user_31:YsDilwYjHapqhZ3W@cluster0-qdnbm.mongodb.net/test?retryWrites=true&w=majority"

    @staticmethod
    def init():
        client = pymongo.MongoClient(DB.URI)
        DB.DATABASE = client.testing_application

    @staticmethod
    def insert(collection, data):
        DB.DATABASE[collection].insert(data)

    @staticmethod
    def find_one(collection, query):
        return JSONEncoder().encode(DB.DATABASE[collection].find_one(query)) 

    @staticmethod
    def find_all(collection):
        res = []        
        temp_list = list(DB.DATABASE[collection].find()) 
        # python dicts to JSON
        for elem in temp_list:
            res.append(JSONEncoder().encode(elem))      
        return res

    @staticmethod
    def delete(collection, query):
        DB.DATABASE[collection].remove(query)


if __name__ == "__main__":    
    DB.init()

    all_elements = DB.find_all(collection='books')
    print(all_elements)
    