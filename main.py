from flask import Flask, jsonify, request
from flask_restful import reqparse, abort, Api, Resource

from pkg.database import DB
from pkg.models.book import Book
from pprint import pprint, pformat
import json
from bson import ObjectId, json_util

app = Flask(__name__)
DB.init()
api = Api(app)

def abort_404(name):
    resp = Book(name).find_one()
    pprint(resp)
    if resp == "null":
        abort(404, message="Book {} doesn't exist".format(name))

parser = reqparse.RequestParser()
parser.add_argument('name', type=str)

# Todo
# shows a single todo item and lets you delete a todo item
class BookItem(Resource):
    def get(self, name):
        abort_404(name)
        book = Book(name)
        data = book.find_one()
        # pprint(data)
        return data
        # return {"name": "name_example", "date": "data_example"}

    def delete(self, todo_id):
        abort_404(todo_id)
        del Books[todo_id]
        return '', 204

    def put(self, todo_id):
        args = parser.parse_args()
        task = {'task': args['task']}
        Books[todo_id] = task
        return task, 201


# shows a list of all Books, and lets you POST to add new tasks
class BookList(Resource):
    def get(self):
        res = Book('test').find_all()
        return res

    # curl -i -H "Content-Type: application/json" -X POST -d "{\"name\":\"abc\"}" 127.0.0.1:5000/
    def post(self):
        args = parser.parse_args()
        rq_data = request.json        
        new_book = Book(name = rq_data['name'])
        print("debug message booklist.post")        
        new_book.insert()
        return rq_data

##
## Actually setup the Api resource routing here
##
api.add_resource(BookList, '/')
api.add_resource(BookItem, '/<name>')


if __name__ == '__main__':
    app.run(debug=True)